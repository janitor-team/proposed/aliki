// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __PROGWIN_H
#define __PROGWIN_H


#include <clxclient.h>


class Progwin : public X_window
{
public:

    Progwin (X_window *parw, X_resman *resman);
    ~Progwin (void);

    void show (int x, int y, const char *text);
    void prog (float v);
    void hide (void);
 
private:

    enum
    {
	XDEF = 350, YDEF = 100,
    };

    virtual void handle_event (XEvent *xe);

    X_textip    *_text;
    X_meter     *_prog;
};


#endif
