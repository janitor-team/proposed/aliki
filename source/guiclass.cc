// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "guiclass.h"


typedef X_button *xbutp;


Radiosel::Radiosel (X_window *parw, X_callback *xcbh) :
    _nbut (0),
    _stat (-1),
    _butt (0),
    _parw (parw),
    _xcbh (xcbh)
{
}


Radiosel::~Radiosel (void)
{
    delete[] _butt;
}
    

void Radiosel::init (X_button_style *bst, X_textln_style *tst, const char **text, 
                     int nbut, int xpos, int ypos, int xsize, int ystep, int cbid)
{
    int i, x, y;
 
    _nbut = nbut;
    _cbid = cbid;
    _butt = new xbutp [nbut]; 
    bst->size.x = 17;
    bst->size.y = 17;
    x = xpos;
    y = ypos;
    for (i = 0; i < nbut; i++)
    {
	_butt [i] = new X_ibutton (_parw, this, bst, x, y, _parw->disp ()->image1515 (X_display::IMG_RT), i);
        _butt [i]->x_map ();
        (new X_textln (_parw, tst, x + 20, y, xsize - 20, 17, text [i], -1))->x_map ();      
        y += ystep;
    }
}


void Radiosel::set_stat (int stat)
{
    if (stat != _stat)
    {
	if (_stat >= 0) _butt [_stat]->set_stat (0);
        _stat = stat;
	if (_stat >= 0) _butt [_stat]->set_stat (1);
    }
}


void Radiosel::handle_callb (int k, X_window *W, _XEvent *E)
{
    switch (X_callback::cb_class (k))
    {
    case X_callback::BUTTON:
        if (X_callback::cb_event (k) == X_button::PRESS)
	{
            set_stat (((X_button *) W)->cbid ());
            if (_xcbh) _xcbh->handle_callb (_cbid, 0, 0);
	}
    }
}

