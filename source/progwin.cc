// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include "progwin.h"
#include "styles.h"



Progwin::Progwin (X_window *parw, X_resman *resman) :
    X_window (parw, 0, 0, XDEF, YDEF, XftColors.main_bg->pixel)
{
    _text = new X_textip (this, 0, &Tst0, 24, 20, 302, 18, 127);
    _text->x_map ();
    _prog = new X_hmeter (this, &Mprogr, &Sprogr, 24, 60, 12);
    _prog->x_map ();
}


Progwin::~Progwin (void)
{
}


void Progwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    default:
	;
    }
}


void Progwin::show (int x, int y, const char *text)
{
    _text->set_text (text);
    _prog->set_val (0.0f);
    x_move (x - XDEF / 2, y - YDEF / 2);
    x_mapraised ();
}


void Progwin::prog (float v)
{
    _prog->set_val (v);
}


void Progwin::hide (void)
{
    x_unmap ();
}


