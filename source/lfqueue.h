// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __LFQUEUE_H
#define __LFQUEUE_H


class Fltlfq
{
public:

    Fltlfq (int size, int chan);
    ~Fltlfq (void); 

    void reset (void) { _nwr = _iwr = _nrd = _ird = 0; }
    int  size (void) const { return _size; } 
    int  chan (void) const { return _chan; } 

    float *wr_ptr (void) const { return _data + (_iwr * _chan); }
    int  wr_frames (void) const { return _size - (_nwr - _nrd); } 
    int  wr_nowrap (int n) const { return (_size - _iwr < n) ? (_size - _iwr) : n; }
    void wr_commit (int n)
    {
        _nwr += n;
        _iwr += n;
        if (_iwr >= _size) _iwr -= _size;       
    }

    float *rd_ptr (void) { return _data + (_ird * _chan); }
    int  rd_frames (void) const { return _nwr - _nrd; } 
    int  rd_nowrap (int n) const { return (_size - _ird < n) ? (_size - _ird) : n; }
    void rd_commit (int n)
    {
        _nrd += n;
        _ird += n;
        if (_ird >= _size) _ird -= _size;       
    }

private:

    int    _size;
    int    _chan;
    int    _nwr;
    int    _iwr;
    int    _nrd;
    int    _ird;
    float *_data;
};


#endif

