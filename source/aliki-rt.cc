// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <clthreads.h>
#include "shdata.h"
#include "mainthr.h"
#include "audiothr.h"


static  Mainthr   *mainthr;
static  Audiothr  *audiothr;


static void sigint_handler (int)
{
    signal (SIGINT, SIG_IGN);
    mainthr->stop ();
}


int main (int ac, char *av [])
{
    Shmem  *shmem;
    Shdata *sdata;

    if (ac < 2) return 1;
    shmem = new Shmem (av [1], SHM_SIZE, false);
    if (shmem->size () == 0) return 1;
    sdata = (Shdata *)(shmem->data ());

    // insert version check here

    mainthr  = new Mainthr (sdata);
    audiothr = new Audiothr (sdata);

    signal (SIGINT, sigint_handler); 
    mainthr->thr_main ();

    delete audiothr; 
    delete mainthr;
    delete shmem;

    return 0;
}
