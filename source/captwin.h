// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __CAPTWIN_H
#define __CAPTWIN_H


#include <clxclient.h>
#include "extproc.h"
#include "shdata.h"
#include "filewin.h"


class Captwin : public X_window, public X_callback
{
public:

    Captwin (X_window *parent, X_resman *resman, X_callback *callb, Filewin *filewin);
    ~Captwin (void);

    bool running (void) const { return _state != EXIT; }
    void show (void);
    void hide (void);
    void stop (void);
    void handle_time (void);
    void set_sess (const char *name);
    int  get_rate (void) const { return _shdata ? _shdata->_fsamp : 0; }

private:

    enum { INIT, STOP, TEST, CAPT, TERM, EXIT };

    enum
    {
      XPOS = 130, YPOS = 130,
      XDEF = 600, YDEF = 100,
    };

    enum { B_SSEL, B_ESEL, B_STOP, B_CAPT, B_TEST, B_TRIG, B_CPCH, B_PLCH = B_CPCH + 8 };

    virtual void handle_event (XEvent *);
    virtual void handle_callb (int, X_window *, XEvent *);

    void xcmesg (XClientMessageEvent *);
    void expose (XExposeEvent *);
    void redraw (void);
    void makewin (void);
    void setstate (int);
    void add_text (int xp, int yp, int xs, int ys, const char *text, int align);
    void addgroup (U32 ftype);
    void remgroup (void);
    void askdata (X_textip *T);
    void starttest (void);
    void startcapt (void);
    void file_load (void);

    Atom            _xatom;
    X_callback     *_callb;
    Filewin        *_filewin;
    int             _xs;
    int             _ys;
    int             _state;
    int             _count;
    Extproc        *_extproc;
    Shdata         *_shdata;

    X_subwin       *_wlevel;
    X_textip       *_grlab [8];
    X_textip       *_chlab [8];
    X_ibutton      *_chbut [8];
    X_meter        *_meter [8];
    X_ibutton      *_bsweep;
    X_ibutton      *_bequal;
    X_textip       *_tlevel;
    X_textip       *_tsweep;
    X_textip       *_tequal;

    X_textip       *_ttplay;
    X_textip       *_ttcapt;
    X_textip       *_tniter;
    X_textip       *_ttskip;
    X_textip       *_titcom;

    X_tbutton      *_bplay [8];
    X_tbutton      *_bstop;
    X_tbutton      *_bcapt;
    X_tbutton      *_btest;
    X_tbutton      *_btrig;
    
    X_menuwin      *_mtype;
    int             _chan0;

    static X_menuwin_item _mtype_def []; 
    static U32 _ftype []; 
};


#endif
