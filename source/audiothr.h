// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <clthreads.h>
#include <zita-alsa-pcmi.h>
#include <jack/jack.h>
#include "shdata.h"


#define DECAY 30.0f   // Meter decay in dB/s


class Audiothr : public P_thread
{
public:

    enum { N_IP = 8, N_OP = 8 };

    Audiothr (Shdata *shdata);
    virtual ~Audiothr (void);

private:

    virtual void thr_main (void);

    void  init_alsa (void);
    void  close_alsa (void);
    void  init_jack (const char *server = 0);
    void  close_jack (void);
    void  jack_shutdown (void);
    int   jack_callback (jack_nframes_t nframes);
    void  init (void);
    void  process (void);

    Shdata         *_shdata;

    volatile bool   _run_alsa;
    Alsa_pcmi      *_alsa_handle;
    Esync           _alsa_sync;

    volatile bool   _run_jack;
    jack_client_t  *_jack_handle;
    jack_port_t    *_jack_ip [N_IP];
    jack_port_t    *_jack_op [N_OP];

    float          *_ip [N_IP];
    float          *_op [N_OP];
    U32             _fsamp;
    S32             _nplay;
    S32             _ncapt;
    U32             _fsize;
    U32             _scnt;
    float           _decay;
  
    static void jack_static_shutdown (void *arg);
    static int  jack_static_callback (jack_nframes_t nframes, void *arg);
};


