// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __INVFILT_H
#define __INVFILT_H


#include <fftw3.h>


class Invfilt
{
public:

    Invfilt (void);
    ~Invfilt (void);

    void fini (void);
    void init (float rate, float tmax); 

    void setwin (float tneg, float thif, float tlof);
    void setinp (float *x, int dlen, int offs); 
    void settarg_log (float *v, int n, float f0, float f1);
    void setbias_log (float *v, int n, float f0, float f1);

private:

    float           rcoswin (float x, float p);

    float           _rate;
    float           _tmax;
    int             _lfft;
    float           _tneg;
    float           _thif;
    float           _tlof;
    float          *_td1;
    float          *_td2;
    float          *_targ;
    float          *_bias;
    fftwf_complex  *_fd1;
    fftwf_complex  *_fd2;
    fftwf_plan      _fwd_plan;
    fftwf_plan      _rev_plan;
};


#endif
