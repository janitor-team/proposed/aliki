// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __EXPTWIN_H
#define __EXPTWIN_H


#include <sndfile.h>
#include "guiclass.h"
#include "extproc.h"
#include "filewin.h"
#include "impdata.h"


class Exptwin : public X_window, public X_callback
{
public:

    Exptwin (X_window *parent, X_resman *resman, X_callback *callb);
    ~Exptwin (void);

    void show (void);
    void hide (int cbarg);
    int type (void) const { return _rtype.stat (); }
    int chan (void) const { return _rchan.stat (); }
    int form (void) const { return _rform.stat (); }
    int sect (void) const { return _rsect.stat (); }
    const char *file (void) const { return _tfile->text (); }


private:

    enum { XPOS = 130, YPOS = 130, XDEF = 500, YDEF = 250 };
    enum { B_FILE, B_EXPT, B_CANC };

    virtual void handle_event (XEvent *);
    virtual void handle_callb (int, X_window *, XEvent *);

    void xcmesg (XClientMessageEvent *);
    void makewin (void);
    void add_text (int xp, int yp, int xs, int ys, const char *text, int align);

    Atom            _xatom;
    X_callback     *_callb;
    Radiosel        _rtype; 
    Radiosel        _rchan; 
    Radiosel        _rsect; 
    Radiosel        _rform; 
    X_ibutton      *_bfile;
    X_tbutton      *_bexpt;
    X_tbutton      *_bcanc;
    X_textip       *_tfile;


    static const char * _rtype_text [4];
    static const char * _rchan_text [2];
    static const char * _rsect_text [3];
    static const char * _rform_text [3];
};


#endif
