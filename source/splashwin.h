// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __SPLASHWIN_H
#define __SPLASHWIN_H


#include <clxclient.h>


class Splashwin : public X_window
{
public:

    Splashwin (X_window *parent, int xp, int yp, int xs, int ys);
    ~Splashwin (void);

private:

    virtual void handle_event (XEvent *);
    void expose (XExposeEvent *);

    int _xs;
    int _ys;
};


#endif
