// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "styles.h"


struct colors      Colors;
struct fonts       Fonts;
struct xft_colors  XftColors;
struct xft_fonts   XftFonts;


X_button_style  Bst0, Bst1, Bst2;
X_textln_style  Tst0, Tst1;
X_menuwin_style Mst1, Mst2; 
X_scale_style   Slevel, Sprogr;
X_meter_style   Mlevel, Mprogr; 
X_mclist_style  Lst0; 


void init_styles (X_display *disp, X_resman *xrm)
{
    Colors.black        = disp->blackpixel ();
    Colors.white        = disp->whitepixel ();
    Colors.main_ds      = disp->alloc_color (xrm->get (".color.main.ds",     "gray20"),    Colors.black);
    Colors.main_ls      = disp->alloc_color (xrm->get (".color.main.ls",     "white"),    Colors.white);
    Colors.text_bg      = disp->alloc_color (xrm->get (".color.text.bg",     "white"),     Colors.white);
    Colors.text_hl      = disp->alloc_color (xrm->get (".color.text.hl",     "#c0ffa0"),   Colors.white);
    Colors.text_mk      = disp->alloc_color (xrm->get (".color.text.mk",     "red"),       Colors.black);
    Colors.spla_bg      = disp->alloc_color (xrm->get (".color.splash.bg",   "white"),     Colors.white);
    Colors.spla_bd      = disp->alloc_color (xrm->get (".color.splash.bd",   "black"),     Colors.black);
    Colors.plot_bg      = disp->alloc_color (xrm->get (".color.plot.bg",     "white"),     Colors.white);
    Colors.plot_gr      = disp->alloc_color (xrm->get (".color.plot.gr",     "gray90"),    Colors.black);
    Colors.plot_da [0]  = disp->alloc_color (xrm->get (".color.plot.da0",    "red"),       Colors.black);
    Colors.plot_da [1]  = disp->alloc_color (xrm->get (".color.plot.da1",    "#00b000"),   Colors.black);
    Colors.plot_mk [0]  = disp->alloc_color (xrm->get (".color.plot.mk0",    "blue"),      Colors.black);
    Colors.plot_mk [1]  = disp->alloc_color (xrm->get (".color.plot.mk1",    "#908000"),   Colors.black);
    Colors.meter_bg     = disp->alloc_color (xrm->get (".color.meter.bg"  ,  "gray10"),    Colors.black);
    Colors.meter_ds     = disp->alloc_color (xrm->get (".color.meter.ds"  ,  "black"),     Colors.black);
    Colors.meter_ls     = disp->alloc_color (xrm->get (".color.meter.ls"  ,  "gray50"),    Colors.white);
    Colors.meter_offc0  = disp->alloc_color (xrm->get (".color.meter.offc0", "gray30"),    Colors.black);
    Colors.meter_offc1  = disp->alloc_color (xrm->get (".color.meter.offc1", "gray30"),    Colors.black);
    Colors.meter_offmk  = disp->alloc_color (xrm->get (".color.meter.offmk", "gray40"),    Colors.white);
    Colors.meter_actc0  = disp->alloc_color (xrm->get (".color.meter.actc0", "green"),     Colors.white);
    Colors.meter_actc1  = disp->alloc_color (xrm->get (".color.meter.actc1", "yellow"),    Colors.white);
    Colors.meter_actmk  = disp->alloc_color (xrm->get (".color.meter.actmk", "white"),     Colors.white);

    XftColors.white       = disp->alloc_xftcolor ("white", 0);
    XftColors.black       = disp->alloc_xftcolor ("black", 0);
    XftColors.main_bg     = disp->alloc_xftcolor (xrm->get (".color.main.bg",   "#ece8dc"), XftColors.white);
    XftColors.main_fg     = disp->alloc_xftcolor (xrm->get (".color.main.fg",   "black"),   XftColors.black);
    XftColors.meter_fg    = disp->alloc_xftcolor (xrm->get (".color.meter.fg",  "#a0a0ff"), XftColors.white);
    XftColors.tick0       = disp->alloc_xftcolor (xrm->get (".color.tick0",     "gray70"),  XftColors.white);
    XftColors.tick1       = disp->alloc_xftcolor (xrm->get (".color.tick1",     "red"),     XftColors.black);
    XftColors.text_fg     = disp->alloc_xftcolor (xrm->get (".color.text.fg",   "black"),   XftColors.black);
    XftColors.spla_fg     = disp->alloc_xftcolor (xrm->get (".color.splash.fg", "blue"),    XftColors.black);
    XftColors.butt_bg [0] = disp->alloc_xftcolor (xrm->get (".color.butt.bg0",  "#e0d8d0"), XftColors.white);
    XftColors.butt_bg [1] = disp->alloc_xftcolor (xrm->get (".color.butt.bg1",  "#8080ff"), XftColors.white);
    XftColors.butt_bg [2] = disp->alloc_xftcolor (xrm->get (".color.butt.bg2",  "#ffff80"), XftColors.white);
    XftColors.butt_bg [3] = disp->alloc_xftcolor (xrm->get (".color.butt.bg3",  "#e0d8d0"), XftColors.white);
    XftColors.butt_fg [0] = disp->alloc_xftcolor (xrm->get (".color.butt.fg0",  "black"),   XftColors.black);
    XftColors.butt_fg [1] = disp->alloc_xftcolor (xrm->get (".color.butt.fg1",  "white"),   XftColors.white);
    XftColors.butt_fg [2] = disp->alloc_xftcolor (xrm->get (".color.butt.fg2",  "black"),   XftColors.black);
    XftColors.butt_fg [3] = disp->alloc_xftcolor (xrm->get (".color.butt.fg3",  "gray60"),  XftColors.white);
    XftColors.menu_bg     = disp->alloc_xftcolor (xrm->get (".color.menu.bg",   "#ece8dc"), XftColors.white);
    XftColors.menu_fg [0] = disp->alloc_xftcolor (xrm->get (".color.menu.fg0",  "black"),   XftColors.black);
    XftColors.menu_fg [1] = disp->alloc_xftcolor (xrm->get (".color.menu.fg1",  "blue"),    XftColors.black);
    XftColors.menu_fg [2] = disp->alloc_xftcolor (xrm->get (".color.menu.fg2",  "red"),     XftColors.black);
    XftColors.menu_fg [3] = disp->alloc_xftcolor (xrm->get (".color.menu.fg3",  "red"),     XftColors.black);
    XftColors.menu_mm     = disp->alloc_xftcolor (xrm->get (".color.menu.mm",   "gray70"),  XftColors.white);
    XftColors.mark_fg     = disp->alloc_xftcolor (xrm->get (".color.plot.mkfg", "white"),   XftColors.white);
    XftColors.plot_sc     = disp->alloc_xftcolor (xrm->get (".color.plot.sc",   "black"),   XftColors.black);
 
    XftFonts.spla1  = disp->alloc_xftfont (xrm->get (".font.splash1", "times:pixelsize=24"));
    XftFonts.spla2  = disp->alloc_xftfont (xrm->get (".font.splash2", "times:pixelsize=14"));
    XftFonts.button = disp->alloc_xftfont (xrm->get (".font.button",  "luxi:pixelsize=13"));
    XftFonts.labels = disp->alloc_xftfont (xrm->get (".font.labels",  "luxi:pixelsize=13"));
    XftFonts.scales = disp->alloc_xftfont (xrm->get (".font.scales",  "luxi:pixelsize=10"));
    XftFonts.files  = disp->alloc_xftfont (xrm->get (".font.files",   "luxi:bold:pixelsize=13"));
    XftFonts.menus  = disp->alloc_xftfont (xrm->get (".font.files",   "luxi:pixelsize=13"));

    Bst0.font = XftFonts.button;
    Bst0.color.bg [0] = XftColors.butt_bg [0]->pixel;
    Bst0.color.fg [0] = XftColors.butt_fg [0];
    Bst0.color.bg [1] = XftColors.butt_bg [1]->pixel;
    Bst0.color.fg [1] = XftColors.butt_fg [1];
    Bst0.color.bg [2] = XftColors.butt_bg [2]->pixel;
    Bst0.color.fg [2] = XftColors.butt_fg [2];
    Bst0.color.bg [3] = XftColors.butt_bg [3]->pixel;
    Bst0.color.fg [3] = XftColors.butt_fg [3];
    Bst0.color.shadow.bgnd = XftColors.main_bg->pixel;
    Bst0.color.shadow.lite = Colors.main_ls;
    Bst0.color.shadow.dark = Colors.main_ds;
    Bst0.type = X_button_style::RAISED;

    Bst1.font = XftFonts.button;
    Bst1.color.bg [0] = XftColors.main_bg->pixel;
    Bst1.color.fg [0] = XftColors.main_fg;
    Bst1.color.bg [1] = XftColors.butt_bg [0]->pixel;
    Bst1.color.fg [1] = XftColors.main_fg;
    Bst1.color.shadow.bgnd = XftColors.main_bg->pixel;
    Bst1.color.shadow.lite = Colors.main_ls;
    Bst1.color.shadow.dark = Colors.main_ds;
    Bst1.type = X_button_style::RAISED;

    Bst2.font = 0;
    Bst2.color.bg [0] = XftColors.main_bg->pixel;
    Bst2.color.fg [0] = XftColors.tick0;
    Bst2.color.bg [1] = XftColors.main_bg->pixel;
    Bst2.color.fg [1] = XftColors.tick1;
    Bst2.type = X_button_style::PLAIN;
    Bst0.size.x = 15;
    Bst0.size.y = 15;

    Tst0.font = XftFonts.labels;
    Tst0.color.normal.bgnd = XftColors.main_bg->pixel;
    Tst0.color.normal.text = XftColors.main_fg;
    Tst0.color.shadow.lite = Colors.main_ls;
    Tst0.color.shadow.dark = Colors.main_ds;
    Tst0.color.shadow.bgnd = XftColors.main_bg->pixel;

    Tst1.font = XftFonts.labels;
    Tst1.color.normal.bgnd = Colors.text_bg;
    Tst1.color.normal.text = XftColors.text_fg;
    Tst1.color.focus.bgnd  = Colors.text_hl; 
    Tst1.color.focus.text  = XftColors.text_fg;
    Tst1.color.focus.line  = Colors.text_mk;
    Tst1.color.shadow.lite = XftColors.main_bg->pixel;
    Tst1.color.shadow.dark = Colors.main_ds;
    Tst1.color.shadow.bgnd = Colors.main_ls;

    Mst1.font = XftFonts.menus;
    Mst1.step = 20;
    Mst1.type = X_menuwin_style::RAISED;
    Mst1.color.bg     = XftColors.menu_bg;
    Mst1.color.fg [0] = XftColors.menu_fg [0];
    Mst1.color.fg [1] = XftColors.menu_fg [1];
    Mst1.color.fg [2] = XftColors.menu_fg [2];
    Mst1.color.fg [3] = XftColors.menu_fg [3];
    Mst1.color.mm     = XftColors.menu_mm;
    Mst1.color.shadow.bgnd = XftColors.main_bg->pixel;
    Mst1.color.shadow.lite = Colors.main_ls;
    Mst1.color.shadow.dark = Colors.main_ds;

    Lst0.font = XftFonts.files;
    Lst0.bg = Colors.white;
    Lst0.fg [0] = XftColors.black;
    Lst0.fg [1] = disp->alloc_xftcolor ("blue",   XftColors.black);
    Lst0.fg [2] = disp->alloc_xftcolor ("purple", XftColors.black);
    Lst0.fg [3] = disp->alloc_xftcolor ("red",    XftColors.black);
    Lst0.dy = Lst0.font->ascent + Lst0.font->descent + 2;

    Mlevel.bg = Colors.meter_bg;
    Mlevel.off.c0 = Colors.meter_offc0;
    Mlevel.off.c1 = Colors.meter_offc1;
    Mlevel.off.mk = Colors.meter_offmk;
    Mlevel.act.c0 = Colors.meter_actc0;
    Mlevel.act.c1 = Colors.meter_actc1;
    Mlevel.act.mk = Colors.meter_actmk;

    Slevel.bg = Colors.meter_bg;
    Slevel.fg = XftColors.meter_fg;
    Slevel.marg = 7;
    Slevel.font = XftFonts.scales;
    Slevel.nseg = 9;
    Slevel.set_tick ( 0,   1, 0.001f, 0    );
    Slevel.set_tick ( 1,  16, 3.16e-3f, "-50");
    Slevel.set_tick ( 2,  46, 1.00e-2f, "-40");
    Slevel.set_tick ( 3,  81, 3.16e-2f, "-30");
    Slevel.set_tick ( 4, 116, 1.00e-1f, "-20");
    Slevel.set_tick ( 5, 141, 1.78e-1f, "-15");
    Slevel.set_tick ( 6, 171, 3.16e-1f, "-10");
    Slevel.set_tick ( 7, 199, 5.00e-1f, "-6" );
    Slevel.set_tick ( 8, 220, 7.07e01f, "-3" );
    Slevel.set_tick ( 9, 241, 1.000f,   "0"  );

    Mprogr.bg = Colors.meter_bg;
    Mprogr.off.c0 = Colors.meter_offc0;
    Mprogr.off.c1 = Colors.meter_offc0;
    Mprogr.off.mk = Colors.meter_offc0;
    Mprogr.act.c0 = Colors.meter_actc0;
    Mprogr.act.c1 = Colors.meter_actc0;
    Mprogr.act.mk = Colors.meter_actc0;

    Sprogr.bg = Colors.meter_bg;
    Sprogr.fg = XftColors.meter_fg;
    Sprogr.marg = 1;
    Sprogr.font = 0;
    Sprogr.nseg = 10;
    Sprogr.set_tick ( 0,   1, 0.0f, 0);
    Sprogr.set_tick ( 1,  31, 0.1f, 0);
    Sprogr.set_tick ( 2,  61, 0.2f, 0);
    Sprogr.set_tick ( 3,  91, 0.3f, 0);
    Sprogr.set_tick ( 4, 121, 0.4f, 0);
    Sprogr.set_tick ( 5, 151, 0.5f, 0);
    Sprogr.set_tick ( 6, 181, 0.6f, 0);
    Sprogr.set_tick ( 7, 211, 0.7f, 0);
    Sprogr.set_tick ( 8, 241, 0.8f, 0);
    Sprogr.set_tick ( 9, 271, 0.9f, 0);
    Sprogr.set_tick (10, 301, 1.0f, 0);
}

