// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2013 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdio.h>
#include "styles.h"
#include "exptwin.h"
#include "global.h"


const char *Exptwin::_rtype_text [4] = 
{
    "Single-channel raw  (pcm)",
    "Single-channel wav  (wav)",
    "Mult-channel wavex  (wav)",
    "Ambisonic wavex (amb)"
};


const char *Exptwin::_rchan_text [2] = 
{
    "Selected channels",
    "All channels"
};


const char *Exptwin::_rsect_text [3] = 
{
    "Current section",
    "All sections in one file",
    "All sections in separate files"
};


const char *Exptwin::_rform_text [3] = 
{
    "16 bit",
    "24 bit",
    "Float"
};


Exptwin::Exptwin (X_window *parent, X_resman *resman, X_callback *callb) :
    X_window (parent, XPOS, YPOS, XDEF, YDEF, XftColors.main_bg->pixel),
    _callb (callb),
    _rtype (this, 0),
    _rchan (this, 0),
    _rsect (this, 0),
    _rform (this, 0)
{
    char     s [1024];
    X_hints  H;

    _xatom = XInternAtom (dpy (), "WM_DELETE_WINDOW", True);
    XSetWMProtocols (dpy (), win (), &_xatom, 1);
    _xatom = XInternAtom (dpy (), "WM_PROTOCOLS", True);

    H.rname (resman->rname ());
    H.rclas (resman->rclas ());
    H.size (XDEF, YDEF);
    H.minsize (XDEF, YDEF);
    H.maxsize (XDEF, YDEF);
    H.position (XPOS, YPOS);
    x_apply (&H); 
    sprintf (s, "%s-%s  [%s] Export", PROGNAME, VERSION, resman->rname ());
    x_set_title (s);

    makewin ();
    _rtype.set_stat (EXP_TYPE_WAV);
    _rchan.set_stat (EXP_CHAN_ALL);
    _rsect.set_stat (EXP_SECT_COMB);
    _rform.set_stat (EXP_FORM_FLT);
}


Exptwin::~Exptwin (void)
{
}


void Exptwin::show (void)
{
    x_mapraised ();
}


void Exptwin::hide (int cbarg)
{
    _callb->handle_callb (cbarg, this, 0);
    x_unmap ();
}


void Exptwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    case ClientMessage:
        xcmesg ((XClientMessageEvent *) E);
        break;
    }
}


void Exptwin::xcmesg (XClientMessageEvent *E)
{
    if (E->message_type == _xatom) hide (CB_FILE_CANC);
}


void Exptwin::handle_callb (int k, X_window *W, _XEvent *E )
{
    X_button *B;

    switch (k)
    {
    case X_callback::BUTTON | X_button::RELSE:
    {
	B = (X_button *) W;
	switch (B->cbid ())
	{
	case B_FILE:
	    break;

	case B_EXPT:
	    hide (CB_FILE_EXPT);
	    break;

	case B_CANC:
	    hide (CB_FILE_CANC);
	    break;
	}
	break;
    }
    case X_callback::TEXTIP | X_textip::BUT:
    {
        XSetInputFocus (dpy (), W->win (), RevertToPointerRoot, CurrentTime);
	break;
    }  
    default:
	;
    }
}


void Exptwin::makewin (void)
{
    _rtype.init (&Bst2, &Tst0, _rtype_text, 4,  25,  20, 200, 20, 0); 
    _rchan.init (&Bst2, &Tst0, _rchan_text, 2,  25, 125, 200, 20, 0);
    _rsect.init (&Bst2, &Tst0, _rsect_text, 3, 250, 105, 200, 20, 0);
    _rform.init (&Bst2, &Tst0, _rform_text, 3, 320,  20, 200, 20, 0);
    add_text (15, 190, 95, 17, "Base name", 1);
    _tfile = new X_textip (this, this, &Tst1, 115, 190, 250, 17, 1023);
    _tfile->x_map ();
//    Bst0.size.x = 17;
//    Bst0.size.y = 17;
//    _bfile = new X_ibutton (this, this, &Bst0, 370, 190, disp ()->image1515 (X_display::IMG_DN), B_FILE);
//    _bfile->x_map ();
    Bst0.size.x = 60;
    Bst0.size.y = 18;
    _bexpt = new X_tbutton (this, this, &Bst0, 360, 220, "Export", 0, B_EXPT);
    _bexpt->set_stat (2);
    _bexpt->x_map ();
    _bcanc = new X_tbutton (this, this, &Bst0, 425, 220, "Cancel", 0, B_CANC);
    _bcanc->set_stat (1);
    _bcanc->x_map ();
}


void Exptwin::add_text (int xp, int yp, int xs, int ys, const char *text, int align)
{
    (new X_textln (this, &Tst0, xp, yp, xs, ys, text, align))->x_map ();
}
